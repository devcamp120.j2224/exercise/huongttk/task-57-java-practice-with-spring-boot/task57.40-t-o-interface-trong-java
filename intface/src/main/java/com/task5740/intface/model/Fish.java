package com.task5740.intface.model;

public class Fish extends Animal {
    private int size;
    private boolean canEat;
    public Fish(int age, String gender,
    int size, boolean canEat) {
        super(age, gender);
        this.size = size;
        this.canEat = canEat;
    }
    public int getSize() {
        return size;
    }
    public void setSize(int size) {
        this.size = size;
    }
    public boolean isCanEat() {
        return canEat;
    }
    public void setCanEat(boolean canEat) {
        this.canEat = canEat;
    }
    @Override
    public boolean isMammal() {
        return false;
    }
    @Override
    public void mate() {
        System.out.println("Fish mates");
    }
    public void swim() {
        System.out.println("Fish swims");
    }
}
