package com.task5740.intface.model;

import java.util.ArrayList;

public abstract class Person implements IAnimal, ILive {
	private int age;
	private String gender;
	private String name;
	private Address address;
	private ArrayList<Animal> listPet;
	// khởi tạo đầy đủ tham số
	public Person(int age, String gender,
	String name, Address address, 
	ArrayList<Animal> listPet) {
		this.age = age;
		this.gender = gender;
		this.name = name;
		this.address = address;
		this.listPet = listPet;
	}
	public abstract void eat();
	public boolean isAnimal() {
		return false;
	};
	public boolean isLive() {
		return true;
	}
	// getter setter
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public ArrayList<Animal> getListPet() {
		return listPet;
	}
	public void setListPet(ArrayList<Animal> listPet) {
		this.listPet = listPet;
	}
}
